#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "piofpetapi.settings")

    # For setting the run environment (`Prod` or `Dev`) use --env=
    args = list(filter(lambda arg: arg[:6] == '--env=', sys.argv[1:]))
    if len(args) > 0:
        env = args[0][6:]
        os.environ.setdefault('DJANGO_CONFIGURATION', env)
        sys.argv.remove(args[0])

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
