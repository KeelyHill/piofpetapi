#!/bin/bash

ARGS=""

ARGS="$ARGS --log-to-terminal"
ARGS="$ARGS --port 8080"
ARGS="$ARGS --url-alias /static piofpetapi/static"

exec mod_wsgi-express start-server $ARGS piofpetapi/wsgi.py
