# Pictures of People Taking Pictures
PicturesOfPeopleTaking.Pictures

## About
Currently hosted on OpenShift, was once hosted on Bluehost via keelyhill.com hosting.

## Backup
This repo mainly acts as a backup, as there are no contributors.
Here is some code not in the repo that can be of importance. (I made this repo at a state of lesser knowledge and I don't feel like migrating these files.) Hopefully this wont be needed.

### Makefile
```bash
clean:
	cd ./build && \
	rm setup.py && \
	rm requirements.txt && \
	rm ./wsgi/application && \
	rm -r ./wsgi/piofpetapi_project


prepare-build:
	mkdir -p ./build/wsgi/piofpetapi_project && \
	cp -r ./piofpetapi/* ./build/wsgi/piofpetapi_project && \
	cp ./openshift-application ./build/wsgi/application && \
	mv ./build/wsgi/piofpetapi_project/setup.py ./build # move setup.py to app root

	# mv ./build/wsgi/piofpetapi_project/requirements.txt ./build && \


prod-deploy: prepare-build
	cd ./build && \
	git add -A && \
 git commit -a -m "Deploy" && \
 git push origin master
```

### Gulpfile

```js
'use strict';

var projectName = 'piofpetapi'; // project name

var dest = function (folder, options) {
 options = options || {};
 options.cwd = projectName;

 return gulp.dest(folder, options);
};

var gulp = require('gulp'),
 // sass = require('gulp-ruby-sass'),
 sass = require('gulp-sass'),
 uglify = require('gulp-uglify'),
 rename = require('gulp-rename'),
 livereload = require('gulp-livereload');
 // delete? for `clean` task

gulp.task('scss', function() {
 gulp.src('src/scss/main.scss') /*no return for proper error handling*/
  .pipe(sass.sync({ outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(dest('static/css'));
});

gulp.task('js', function() {
 return gulp.src('src/js/**/*.js') // try with this too
 // return gulp.src('src/js/*.js')
  .pipe(uglify())
  .pipe(rename({ suffix: '.min' }))
  .pipe(dest('static/js'))

});

gulp.task('watch', function() {
 process.chdir('piofpetapi');

 // Watch src files
 gulp.watch('src/scss/**/*.scss', ['scss']);
 gulp.watch('src/js/**/*.js', ['js']);

 // IDK if i want to do it with templates too? v
 gulp.watch([projectName + '/static/**', '**/templates/**/**']).on('change', livereload.changed);

 livereload.listen();
});
```

### OpenShift Application
```python
#!/usr/bin/env python

import os
import sys

## GETTING-STARTED: make sure the next line points to your settings.py:
os.environ['ON_OPENSHIFT'] = 'True'
os.environ['DJANGO_SETTINGS_MODULE'] = 'piofpetapi.settings'
## GETTING-STARTED: make sure the next line points to your django project dir:
sys.path.append(os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'wsgi', 'piofpetapi_project'))
from distutils.sysconfig import get_python_lib
os.environ['PYTHON_EGG_CACHE'] = get_python_lib()

import django.core.wsgi
application = django.core.wsgi.get_wsgi_application()
```

### SVG Icon
```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?> <!-- Created with Inkscape (http://www.inkscape.org/) -->  <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="260" height="260" id="svg2985" version="1.1" inkscape:version="0.48.5 r10040" sodipodi:docname="piofpetapi_icon_maybe.svg" inkscape:export-filename="/Users/Keely/Developer/piofpetapi/piofpetapi_icon_maybe.png" inkscape:export-xdpi="90" inkscape:export-ydpi="90">   <defs   id="defs2987">  <inkscape:path-effect  effect="spiro"  id="path-effect3482"  is_visible="true" />   </defs>   <sodipodi:namedview   id="base"   pagecolor="#ffffff"   bordercolor="#666666"   borderopacity="1.0"   inkscape:pageopacity="0.0"   inkscape:pageshadow="2"   inkscape:zoom="2.4923077"   inkscape:cx="130"   inkscape:cy="130"   inkscape:current-layer="layer1"   showgrid="false"   inkscape:document-units="px"   inkscape:grid-bbox="true"   inkscape:showpageshadow="false"   inkscape:window-width="1440"   inkscape:window-height="855"   inkscape:window-x="0"   inkscape:window-y="301"   inkscape:window-maximized="1"   showborder="false"   borderlayer="true">  <inkscape:grid  type="xygrid"  id="grid4340"  empspacing="5"  visible="true"  enabled="true"  snapvisiblegridlinesonly="true" />   </sodipodi:namedview>   <metadata   id="metadata2990">  <rdf:RDF>    <cc:Work    rdf:about="">   <dc:format>image/svg+xml</dc:format>   <dc:type   rdf:resource="http://purl.org/dc/dcmitype/StillImage" />   <dc:title></dc:title>    </cc:Work>  </rdf:RDF>   </metadata>   <g   inkscape:groupmode="layer"   id="layer2"   inkscape:label="Back"   style="display:inline">  <rect  style="fill:#00d2ff;fill-opacity:1;stroke:none"  id="rect4336"  width="260"  height="260"  x="0"  y="-2.797762e-14" />   </g>   <g   id="layer1"   inkscape:label="Layer 1"   inkscape:groupmode="layer"   transform="translate(0,196)">  <rect  style="fill:#000000;fill-opacity:1;stroke:none"  id="rect3476"  width="316.04764"  height="214.85066"  x="7.8850555"  y="-161.13811" />  <rect  style="fill:#000000;fill-opacity:1;stroke:none"  id="rect3476-2"  width="93.444954"  height="31.883434"  x="32.2612"  y="-181.17488" /> <rect  style="fill:#00d2ff;fill-opacity:1;stroke:#000000;stroke-width:7.49257039999999996;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none"  id="rect3476-2-1"  width="207.35793"  height="169.09328"  x="18.083963"  y="-133.89128" />  <path  sodipodi:type="arc"  style="fill:none;stroke:#d00000;stroke-width:16.75898361;stroke-linecap:round;stroke-linejoin:bevel;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none"  id="path4302"  sodipodi:cx="214.44185"  sodipodi:cy="156.60333"  sodipodi:rx="20.679333"  sodipodi:ry="20.679333"  d="m 235.12118,156.60333 a 20.679333,20.679333 0 1 1 -41.35866,0 20.679333,20.679333 0 1 1 41.35866,0 z"  transform="matrix(1.2990389,0,0,1.2990389,-4.062305,-246.32217)" />  <rect  style="fill:#000000;fill-opacity:1;stroke:none"  id="rect4304"  width="176.22861"  height="122.87222"  x="33.458626"  y="-100.79071" />  <path  sodipodi:type="arc"  style="fill:none;stroke:#c9c9c9;stroke-width:5;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none"  id="path4308"  sodipodi:cx="107.92912"  sodipodi:cy="169.63416"  sodipodi:rx="17.279989"  sodipodi:ry="17.279989"  d="m 125.20911,169.63416 a 17.279989,17.279989 0 1 1 -34.559976,0 17.279989,17.279989 0 1 1 34.559976,0 z"  transform="matrix(2.1233581,0,0,2.1233581,-107.59925,-399.54866)" />  <rect  style="fill:#000000;fill-opacity:1;stroke:none"  id="rect4310"  width="45.246334"  height="14.159618"  x="46.998497"  y="-113.4559" />  <path  sodipodi:type="arc"  style="fill:#b4b4b4;fill-opacity:1;stroke:none"  id="path4531"  sodipodi:cx="94.290123"  sodipodi:cy="162.5"  sodipodi:rx="6.8209877"  sodipodi:ry="5.2160492"  d="m 101.11111,162.5 a 6.8209877,5.2160492 0 1 1 -13.641975,0 6.8209877,5.2160492 0 1 1 13.641975,0 z"  transform="matrix(1.4660633,0,0,1.9171598,-16.662359,-350.89307)" />   </g>
```
