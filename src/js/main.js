$(function(){
    $('#bitcoin').popover({ title: 'Bitcoin Address QR', trigger: 'hover', html:true,
        content: "<img src='data:image/gif;base64,R0lGODlhhACEAJEAAAAAAP///wAAAAAAACH5BAEAAAIALAAAAACEAIQAAAL/jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8chQNf2jedIDjA29nvwhsTepIi87XSLoMXZSEqNkmlyifPVgFuhtUj5gg88LY179orLx+7TnUCG4WwFdJ6uwvHU+FifF1DnF8hneHFH9hcBNYjVdwgI2VaoSBQpyGS3Rznp0LiYuFlpACpm1idn6RlFF8o5ymr6hfo4KNoaqFoKSyjretoEPPSIuZtJ6lu7arXMe7lq/Kqrqcw8VRx9naUtaVs9ixuO3Y2cOs3qbH47LN5O/nxODR0vTW/+nTwulV2/rVQOwj6A/uINZJesH75/s+wRK4fwSkBh8+Q9LMjoHTeM/xGP4VJ4cJjDiPkseuwFUiPBhd4QiZxoENzLWGjSUbT5aSZLXyRh7sQkEKUaiwpLNvT5cSjQTin1oSvqM2jCm2tWHuWo8+q6qkJjcc0KtmJTk19x8iw7Ni3EsGWnekW7VmxconPbysBYEmpMuXdHuNv485/evh3+Wu3Vk/AMxDKdAk6qAvLOZgyfVg5Gk6nSy4cxT27L2K21nBVPeg7pebA6dZT3SsQK+pfovJthm+Uj2Wjj24Jrf57dKTdnrVJXstbJwdFQ07xdF1frQTnp14FbZnZtWLdjum/4VsjO+fg96N+Ra76+FfBY6b+bj66ecbt22WTVz62p+ub8gcVRc/8/6x174lEnYG/YmedcXV2911p6ZnW0GypaFYhegxQ6ON9giWF4HoPwcIiXS48tkh9666FDnIidXfTec/oNp1KJ8a3InnAh2sZcit3RWI2MA+JHVYRIxWZchza2GN59QcYG5I0zSiikk0QWaR18Lr41onf7rZGcZQl+6aF9YGq4YH9RgojmiduBZ6KKG0Ion4EA0ream2fCSaectLXp5JVpvjimnG/2CGgG/PlW424TEspneaUteCiW9f134ZEDShbpnHu2h2mZvpk53pL/kXdhl8AdKKamQwbI6GKnOljpmaS2CsKRDUKppZKBRsflp3r+Ol1VptLi665/BturBsL/Fntss7gSq2yyudBnmK1wibAoq1kSqFKnue4466Pisuifn5YWmu2kW2rL4o8bpKvmpIOGWqGnVV4ao6jr5tlok9Numui2x5D5qp/4jvvsvnX2qaK5iobGI73wWbvts/55K++1hsqaJKpUhjmlewmvSu6HB4M2rI6wdkurgkwqpqqkmcqIZ18Bl5wqxjO/cHPNhY5sLAs9PxwnsggHpzFzFudrG9Az2tXxxCiy5TSoXGVIss9N64v0V1hLOTW7VTNrMIxHgy3ahkvda7a6JFvt488dKiwoxHBHRTazFnLMMJjn4ngngiunTWStLIuddcse3/ru4ZQKPrCoy/I6LryJqqe6b+TRVi644UVbOTeiXpbAJuhsT/e16YXZjffp/0b9t6P8ql426rCzvnjg305bqssrnqy0rk8rDrLavge/tSS9A14xutJarjzxcfcrd/Rr4u5s9U+Gbj212C9Pu/YLD3/95+DHTrHIwDa7s9FX182P9niqbPXkaDKOdsS/2+k1/NQRrD/6LSdpRGvX5TK2O5gpcIEMbKADHwjBCEpwghSsoAUviMEMPrAAADs='>"
    });
});

function imageAddSubmit(e) {
    var data = new FormData(this);
    var form =  $(this);
    var submitButton = $('#image_add_form #submit-button');

    submitButton.button('loading');

    $('#errors').html(null);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function(data){
            submitButton.button('reset');

            if (!data.success) {
                var errors = data.detail;
                for (key in errors) {
                    for (message in errors[key]) {
                        $('#errors').append(errors[key][message] + '<br>');
                    }
                }
            } else { // on success
                $('#errors').parent().removeClass('has-error').addClass('has-success');
                $('#errors').append(data.message);
                form[0].reset();
            }
        },
        error: function(http) {
             $('#errors').append(JSON.parse(http.responseText));
        }
    });
    e.preventDefault();
}

$('#image_add_form').submit(imageAddSubmit);

function lgImagePopupInit(imageSelector) {
    $('body').prepend("<div id='lgImgPopup'><img><span class='close' aria-hidden='true'>&times;</span></div>");

    $(document).on('click', imageSelector, function(e) {
        var imgsrc = $(this).attr('data-imgsrc');

        $('#lgImgPopup img').attr('src',imgsrc);
        $('#lgImgPopup').fadeIn(150);
    });

    $('#lgImgPopup .close').click(function(){
        $('#lgImgPopup').fadeOut(150);
    });
}

lgImagePopupInit('#sq-grid .img-is');
// lgImagePopupInit('#photo-grid img');

function loadMoreOnBottom() {
    var wind = $(window);
    var sqGrid = $('#sq-grid');
    var endOfLoad = $('#end-of-load');
    var end = false;

    var page = 1; 
    var loading = false;

    function toggleLoad(to) {
        loading = to;
        if (to)
            endOfLoad.addClass('glyphicon-camera spin');
        else endOfLoad.removeClass('glyphicon-camera spin');
    }

    function doLoad() {
        toggleLoad(true);
        page++;
        
        $.get('/' + page, function(data){
            toggleLoad(false);

            if (data == 'end') {
                end = true;
                endOfLoad.addClass('glyphicon-stop').removeClass('spin');
            } else {
                sqGrid.append(data);
            }
        });
    }

    wind.scroll(function () { 
        if (wind.scrollTop() >= $(document).height() - wind.height() - 300 && !loading && !end) {
            doLoad();
        }
    });

    $('#end-of-load').click(function(){
        if (!loading && !end)
            doLoad();
    });
}

loadMoreOnBottom();

