#!/usr/bin/env python

from setuptools import setup

setup(
    # GETTING-STARTED: set your app name:
    name='PiOfPeTaPi',
    # GETTING-STARTED: set your app version:
    version='1.0',
    # GETTING-STARTED: set your app description:
    description='Pictures of People Taking Pictures',
    # GETTING-STARTED: set author name (your name):
    author='Keely Hill',
    # GETTING-STARTED: set author email (your email):
    author_email='keely@keelyhill.com',
    # GETTING-STARTED: set author url (your url):
    url='http://www.python.org/sigs/distutils-sig/',
    # GETTING-STARTED: define required django version:
    install_requires=[
        'Django==1.8.5',
        'requests==2.8.1'
    ],
    dependency_links=[
        'https://pypi.python.org/simple/django/'
    ],
)
