from django import forms

from django.core.files.uploadedfile import InMemoryUploadedFile as InMemFile

import requests
from PIL import Image, ExifTags
from io import BytesIO

from piofpetapi.settings import reCAPTCHA_secret


class ImageAddForm(forms.Form):

    upload_disabled = True # limited space on HD, make false when I have money/cdn
    upload_disabled_string = 'upload currently disabled, use <a href="//imgur.com/tools">Imgur</a> instead'

    url = forms.URLField(label='<span title="Imgur?">URL</span>', required=False)

    imgfile = forms.ImageField(
        label='<del>or<br><br>Select a file</del> (%s)' % upload_disabled_string,
        required=False,
        # disabled=upload_disabled
    )

    def clean(self):
        captcha_payload = {
            'secret': reCAPTCHA_secret,
            'response': self.data.get('g-recaptcha-response')
        }
        captcha_request = requests.post('https://www.google.com/recaptcha/api/siteverify',
                                        params=captcha_payload).json()

        if(not captcha_request['success']):
            error_codes = captcha_request['error-codes']

            if 'missing-input-response' in error_codes:
                error_message = 'Please complete the reCAPTCHA.'
            elif 'invalid-input-response' in error_codes:
                error_message = 'Try reCAPTCHA again, you appear to be a robot.'
            else:
                error_message = error_codes

            raise forms.ValidationError(error_message, code='captcha_error')

        url = self.cleaned_data.get('url')
        imgfile = self.cleaned_data.get('imgfile')

        # Basic 'is it there' catches
        if upload_disabled and imgfile:
            raise forms.ValidationError(upload_disabled_string, code='option disabled')

        if url and imgfile:
            raise forms.ValidationError('Enter only one.', code='too_much')
        elif not url and not imgfile:
            raise forms.ValidationError('No picture supplied.', code='required')

        # Check file type and process if jpeg.
        if imgfile:
            if imgfile.content_type not in ['image/jpeg', 'image/png']:
                raise forms.ValidationError('Must be a jpeg or png.', code='improper_file')
            if imgfile.content_type == 'image/jpeg':
                # Compress
                image = Image.open(imgfile.file)

                if image._getexif():
                    for orientation in ExifTags.TAGS.keys():
                        if ExifTags.TAGS[orientation] == 'Orientation':
                            break

                    exif = dict(image._getexif().items())

                    if exif[orientation] == 3:
                        image = image.rotate(180, expand=True)
                    elif exif[orientation] == 6:
                        image = image.rotate(270, expand=True)
                    elif exif[orientation] == 8:
                        image = image.rotate(90, expand=True)

                if image.size[0] > 2000 or imgfile.size > 1100000:  # 1.1MB
                    new_size = (int(image.size[0] / 1.5), int(image.size[1] / 1.5))
                    image.resize(new_size, Image.ANTIALIAS)

                output = BytesIO()
                image.save(output, format='JPEG', quality=65)
                output.seek(0)

                imgfile.file = InMemFile(output, 'ImageField',
                                         imgfile.name, 'image/jpeg',
                                         output.getbuffer().nbytes, None)

        return self.cleaned_data
