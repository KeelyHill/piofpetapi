import os
import datetime

from django.db import models
from django.core.cache import cache

from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


class Picture(models.Model):

    def generate_upload_path(self, filename):
        filename, ext = os.path.splitext(filename.lower())
        now = datetime.datetime.now()
        return 'pictures/p' + now.strftime('%y%m%d%f') + ext

    image_file = models.ImageField(upload_to=generate_upload_path, blank=True)
    image_url = models.CharField(max_length=200, blank=True)
    approved = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = "Picture"
        verbose_name_plural = "Pictures"

    def save(self, *args, **kwargs):

        # TODO: Make this un - hacky

        if self.approved:
            cache.clear()

        super(Picture, self).save(*args, **kwargs)

        if self.image_file:
            self.image_url = self.image_file.url

        super(Picture, self).save(*args, **kwargs)


# Delete the actual file on Model/DB delete.
@receiver(pre_delete, sender=Picture)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.image_file.delete(False)
