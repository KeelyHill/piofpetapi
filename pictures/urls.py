from django.conf.urls import url
from django.views.decorators.cache import cache_page
from django.views.generic.base import RedirectView

from .views import HomeView, SubmitView, TesterView

CHACHE_EXPIRE_MINS = 60

cached_home_view = cache_page(
    60 * CHACHE_EXPIRE_MINS)(HomeView.as_view())

urlpatterns = [
    url(r'^$', cached_home_view, {'page': 1}, name='Home'),
    url(r'^(?P<page>[1-9][0-9]*)$', cached_home_view, name='next_page'),

    url(r'^1$', RedirectView.as_view(pattern_name='Home', permanent=True)),
    url(r'^submit/$', cache_page(60 * 60 * 24)(SubmitView.as_view()), name='Submit View'),
    url(r'^test/$', TesterView.as_view(), name='Tester View'),
]
