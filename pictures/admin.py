from django.contrib import admin
from .models import Picture


class PictureAdmin(admin.ModelAdmin):
    list_display = ('approved', 'image_file', 'image_url', 'created')
    ordering = ['approved']
    actions = ['approve_pic']

    def approve_pic(self, request, queryset):
        queryset.update(approved=True)
    approve_pic.short_description = "Approve selected pictures"

admin.site.register(Picture, PictureAdmin)
