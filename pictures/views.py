from django.shortcuts import render

from django.http import HttpResponse, JsonResponse
from django.views.generic import View

from .models import Picture
from .forms import ImageAddForm


pictures_per_page_load = 10
# at least 8 for production


class HomeView(View):

    def get(self, request, *args, **kwargs):

        page_num = int(kwargs['page']) - 1

        offset = pictures_per_page_load * page_num
        limit = offset + pictures_per_page_load

        picture_urls = Picture.objects.filter(
            approved=True).exclude(image_url='').values_list('image_url')[offset:limit]

        if not picture_urls and page_num > 0:
            return HttpResponse('end')

        if page_num > 0:
            return render(request,
                          'pictures/picture_grid.html', {'picture_urls': picture_urls})

        return render(request, 'pictures/home.html', {'picture_urls': picture_urls})


class SubmitView(View):

    def post(self, request):
        form = ImageAddForm(request.POST, request.FILES)

        if form.is_valid():
            data = form.cleaned_data

            if data['imgfile']:
                new_pic = Picture(image_file=request.FILES['imgfile'])
            else:
                new_pic = Picture(image_url=data['url'])

            new_pic.save()

            return JsonResponse({'message': 'Success!<br><br>Note: The picture must be'
                                 ' approved internally before publicizing', 'success': True})
        else:
            return JsonResponse({'detail': form.errors, 'success': False})

    def get(self, request, *args, **kwargs):
        form = ImageAddForm()
        return render(request, 'pictures/submit.html', {'form': form})


class TesterView(View):

    def get(self, request):
        return HttpResponse('<meta name="robots" content="noindex,nofollow">'
                            'If you see this, normal requests are working.')
