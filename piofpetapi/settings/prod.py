import os
from .common import *
import json

DEBUG = False

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
DJ_PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(DJ_PROJECT_DIR)
WSGI_DIR = os.path.dirname(BASE_DIR)
REPO_DIR = os.path.dirname(WSGI_DIR)
DATA_DIR = os.environ.get('OPENSHIFT_DATA_DIR', BASE_DIR)

# import sys
# sys.path.append(os.path.join(REPO_DIR, 'libs'))
# import secrets
SECRETS = {}

try:
    with open(os.path.join(DATA_DIR, 'secrets.json')) as handle:
        SECRETS = json.load(handle)
except IOError:
    pass

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = SECRETS['secret_key']
reCAPTCHA_secret = SECRETS['reCAPTCHA_secret']

from socket import gethostname
ALLOWED_HOSTS = [
    gethostname(), # For internal OpenShift load balancer security purposes.
    os.environ.get('OPENSHIFT_APP_DNS'), # Dynamically map to the OpenShift gear name.
    'picturesofpeopletaking.pictures', # First DNS alias (set up in the app)
    'www.picturesofpeopletaking.pictures', # Second DNS alias (set up in the app)
]

# Determines how log db connection stays open (seconds)
CONN_MAX_AGE = 60

# Security things

CSRF_COOKIE_SECURE = True # Only can login to admin if SSL
SESSION_COOKIE_SECURE = True

# CSRF_COOKIE_SECURE = False
# SESSION_COOKIE_SECURE = False

SECURE_BROWSER_XSS_FILTER = True
# SECURE_SSL_REDIRECT = True
SECURE_CONTENT_TYPE_NOSNIFF = True
CSRF_COOKIE_HTTPONLY = True
X_FRAME_OPTIONS = 'DENY'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(REPO_DIR, 'static')

MEDIA_ROOT = os.path.join(os.environ.get('OPENSHIFT_DATA_DIR', ''), 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static'),
)

DB_NAME = os.environ['OPENSHIFT_APP_NAME']
DB_USER = os.environ['OPENSHIFT_POSTGRESQL_DB_USERNAME']
DB_PASSWD = os.environ['OPENSHIFT_POSTGRESQL_DB_PASSWORD']
DB_HOST = os.environ['OPENSHIFT_POSTGRESQL_DB_HOST']
DB_PORT = os.environ['OPENSHIFT_POSTGRESQL_DB_PORT']

DATABASES = {
    'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': DB_NAME,               # Or path to database file if using sqlite3.
            'USER': DB_USER,               # Not used with sqlite3.
            'PASSWORD': DB_PASSWD,         # Not used with sqlite3.
            'HOST': DB_HOST,               # Set to empty string for localhost. Not used with sqlite3.
            'PORT': DB_PORT,               # Set to empty string for default. Not used with sqlite3.
        }
}

# Cache

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'default-file': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, 'django_cache'),
    },
    'dummy': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },
}
