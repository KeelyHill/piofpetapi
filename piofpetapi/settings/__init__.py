import os


class ImproperRunConfig(Exception):

    def __init__(self):
        super(Exception, self).__init__(
            '\n\n! Configuration environment not set.\n'
            '! Set the environ `DJANGO_CONFIGURATION`\n'
            '! Use --env=[Dev, Prod] if running manage.py.\n')

runconfig = ''
try:
    runconfig = os.environ['DJANGO_CONFIGURATION']
except KeyError:
    pass

try:
    if os.environ['ON_OPENSHIFT'] == 'True':
        runconfig = 'Prod'
except KeyError:
    pass

if runconfig == 'Dev':
    from .dev import *
elif runconfig == 'Prod':
    from .prod import *
else:
    raise ImproperRunConfig()
