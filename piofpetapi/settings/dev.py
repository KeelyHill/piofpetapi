from .common import *
import json

DEBUG = True

with open(os.path.join(os.path.dirname(__file__), '../../../dev_secrets.json')) as data:
    secrets = json.load(data)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = secrets['key']
reCAPTCHA_secret = secrets['reCAPTCHA_secret']

# STATIC_ROOT = '/Users/Keely/Developer/piofpetapi/piofpetapi/static'
STATIC_URL = '/static/'

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
)

MEDIA_ROOT = '/Users/Keely/Developer/piofpetapi/piofpetapi/media'
MEDIA_URL = '/media/'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },
    'default-file': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, 'django_cache'),
    }
}
