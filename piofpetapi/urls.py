"""piofpetapi URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.http import HttpResponse

from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static

from . import settings


robots_txt = """\
User-agent: *
Disallow: /admin/
Disallow: /test/"""

urlpatterns = [
    url(r'^', include('pictures.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots.txt$', lambda r: HttpResponse(robots_txt, content_type='text/plain')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
